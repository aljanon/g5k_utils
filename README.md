# g5k utils

A collection of small utilities for managing g5k jobs and resources.

## Job Management

- jobids: Lists the user jobs' ids. Note: has _all_ jobs, not only the running
  ones.
- jobconnect: Connects to the job with the given index in the `jobids` list.
  Defaults to 0.

## Nodes

- nodeaddr: Lists node addresses for the current job. Note: needs to be
  connected to the job for this to work.
- nodeconnect: Connects to the node with the given index in the `nodeaddr` list.
  Defaults to 0. The second (optional) parameter is the user name (which
  defaults to the current user)

## Nodes

- kavaddr: Lists node addresses for the current job. Note: needs to be connected
  to the job for this to work.
- kavconnect: Connects to the node with the given index in the `kavaddr` list.
  Defaults to 0. The second (optional) parameter is the user name (which
  defaults to the current user)

## Misc

- home_clean: Removes OAR.* and oarapi.* files

